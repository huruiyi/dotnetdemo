﻿using System.Threading.Tasks;

namespace Quartz.Examples
{
    internal static class TaskUtil
    {
        public static readonly Task CompletedTask = Task.FromResult(true);
    }
}