using log4net;
using Quartz.Impl;
using System;
using System.Threading.Tasks;

namespace Quartz.Examples.Example01
{
    public class SimpleJobSchedulerExample : IExample
    {
        public virtual async Task Run()
        {
            ILog log = LogManager.GetLogger(typeof(SimpleJobSchedulerExample));

            log.Info("------- Initializing ----------------------");

            ISchedulerFactory sf = new StdSchedulerFactory();
            IScheduler sched = await sf.GetScheduler();

            log.Info("------- Initialization Complete -----------");

            DateTimeOffset runTime = DateBuilder.EvenMinuteDate(DateTimeOffset.UtcNow);

            log.Info("------- Scheduling Job  -------------------");

            IJobDetail job = JobBuilder
                .Create<HelloJob>()
                .WithIdentity("job1", "group1")
                .Build();

            ITrigger trigger = TriggerBuilder
                .Create()
                .WithIdentity("trigger1", "group1")
                .StartAt(runTime)
                .Build();

            await sched.ScheduleJob(job, trigger);
            log.Info($"{job.Key} will run at: {runTime:r}");

            await sched.Start();
            log.Info("------- Started Scheduler -----------------");

            log.Info("------- Waiting 65 seconds... -------------");

            await Task.Delay(TimeSpan.FromSeconds(65));

            log.Info("------- Shutting Down ---------------------");
            await sched.Shutdown(true);
            log.Info("------- Shutdown Complete -----------------");
        }
    }
}