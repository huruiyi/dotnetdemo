 
using System;
using System.Threading.Tasks;
using log4net;
using Quartz.Logging;

namespace Quartz.Examples.Example01
{
     
    public class HelloJob : IJob
    {
      private  ILog log = LogManager.GetLogger(typeof(SimpleJobSchedulerExample));

      
        /// </summary>
        public virtual Task Execute(IJobExecutionContext context)
        {
            log.Info($"Hello World! - {DateTime.Now:r}");
            return TaskUtil.CompletedTask;
        }
    }
}