#include"Point.h"
#include"Circle.h"
#include <iostream>
using namespace std;

int main()
{
	Point point1;
	point1.setPoint(10, 20);
	point1.printPoint();

	Point* point2 = new Point;
	point2->setPoint(20, 40);
	point2->printPoint();

	//Point* point3;

	//point3 = &point1;
	//point3->printPoint();

	//point3 = point2;
	//point3->printPoint();

	cout << "*********************************" << endl;
	Circle c1;
	c1.setPoint(point1);
	c1.printPoint();
	cout << "*********************************" << endl;

	return	0;
}