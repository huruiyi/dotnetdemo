#pragma once
#include<iostream>
#include<cstdio>

class Point
{
public:
	void setPoint(int x, int y);
	void printPoint();
	virtual void printInfo();

	~Point();
private:
	int xPos;
	int yPos;
};