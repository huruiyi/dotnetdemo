#pragma once
#include<iostream>
#include<cstdio>
#include"Point.h"

class Circle
{
public:
	void setPoint(Point);
	void printPoint();

private:
	Point point;
};