#include<cstdio>
#include<iostream>

using namespace std;
int main1() {
	cout << "Hello World" << endl;
	char str[20] = "Hello World";

	cout << str[0] << endl;
	cout << sizeof(str) << endl;
	cout << strlen(str) << endl;
	cout << strnlen(str, 100) << endl;
	cout << &str << endl;

	for (size_t i = 0; i < sizeof(str); i++)
	{
		cout << "[" << &str[i] << "]---" << i << endl;
	}
	return EXIT_SUCCESS;
}