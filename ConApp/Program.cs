﻿using CacheApp.Infrastructure;
using ConApp.Model;
using HtmlAgilityPack;
using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace ConApp
{
    class Program
    {
        private static void WriteLine(params object[] values)
        {
            foreach (var item in values)
            {
                Console.Write(item.ToString() + " ");
            }
            Console.WriteLine();
        }

        public static HtmlDocument GetDocument(String text)
        {
            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(text);
            return htmlDocument;
        }

        private static void ConsoleToFile()
        {
            StreamWriter sw = new StreamWriter(@"ConsoleOutput.txt");
            Console.SetOut(sw);

            string str = File.ReadAllText("D:\\aaa.txt", Encoding.Default);
            str = Regex.Replace(str, "\r|\n", "");
            Console.WriteLine(str);
            Console.WriteLine(DateTime.Today);

            sw.Flush();
            sw.Close();
        }

        public static void StreamWriterSetOut()
        {
            StreamWriter sw = new StreamWriter(@"ConsoleOutput.txt");
            Console.SetOut(sw);

            Console.WriteLine("Here is the result:");
            Console.WriteLine("Processing......");
            Console.WriteLine("OK!");

            sw.Flush();
            sw.Close();

            //控制台输出重定向: > F:\ConsoleOutput.txt
        }

        public static void ConcurrentBagList()
        {
            ConcurrentBag<List<int>> clist = new ConcurrentBag<List<int>>();
            for (int i = 0; i < 100000; i++)
            {
            }
        }

        private static void AdoNetDemo1()
        {
            string conStr = "Data Source=.;UID=sa;PWD=sa";
            using (SqlConnection connection = new SqlConnection(conStr))
            {
                connection.Open();
                Console.WriteLine(connection.State + "  " + connection.ClientConnectionId);

                string cmdText = "Select * from [DemoDb].[dbo].[WCT_MANUAL_ACT]  WHERE ID BETWEEN 1 AND 5;" +
                                 "Select * from [DemoDb].[dbo].[WCT_MANUAL_ACT]  WHERE ID BETWEEN 6 AND 10;";
                using (SqlCommand command = new SqlCommand(cmdText, connection))
                {
                    IDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Console.WriteLine(reader["Id"] + "  " + reader["XH_CD"]);
                    }

                    Console.WriteLine("********************************************************");
                    if (reader.NextResult())
                    {
                        while (reader.Read())
                        {
                            Console.WriteLine(reader["Id"] + "  " + reader["XH_CD"]);
                        }
                    }
                }
            }
        }

        private static async Task Main1()
        {
            await Task.Run(() => Console.WriteLine("Hello World!"));
        }

        private static void Main2()
        {
            //https://docs.microsoft.com/zh-cn/dotnet/api/system.threading.threadlocal-1?view=netframework-4.8
            ThreadLocal<Person> threadLocal = new ThreadLocal<Person>();
            Console.WriteLine(threadLocal.IsValueCreated);

            threadLocal.Value = new Person { Id = 1, Name = "Tom" };

            Console.WriteLine(threadLocal.IsValueCreated);
        }

        public static void Main3()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            List<String> citytables = new List<string>
            {
                "http://www.stats.gov.cn/tjsj/tjbz/tjyqhdmhcxhfdm/2018/12.html"
            };

            String className = "citytable";
            Stream stream = WebRequest.Create(citytables[0]).GetResponse().GetResponseStream();
            StreamReader reader = new StreamReader(stream, Encoding.GetEncoding("gb2312"));
            String html = reader.ReadToEnd();

            HtmlDocument htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(html);
            HtmlNodeCollection collections = htmlDocument.DocumentNode.SelectNodes("//table[@class='citytable']");
            HtmlNode node = collections[0];

            htmlDocument = new HtmlDocument();
            htmlDocument.LoadHtml(node.InnerHtml);
            collections = htmlDocument.DocumentNode.SelectNodes("//tr");
            foreach (HtmlNode item in collections)
            {
                htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(item.InnerHtml);

                collections = htmlDocument.DocumentNode.SelectNodes("//td"); ;
                if (className.Equals("citytable"))
                {
                    HtmlNode nodeA = GetDocument(collections[0].InnerHtml).DocumentNode.SelectSingleNode("//a");
                    if (nodeA != null)
                    {
                        Console.WriteLine(nodeA.Attributes[0].Value); ;
                    }
                    Console.WriteLine(collections[0].GetAttributeValue("href", "") + "\t" + collections[0].InnerText + "\t" + collections[1].InnerText);
                }
            }
            Console.WriteLine();
        }
     
        public static void Main4_Redis()
        {
            var cache = RedisTestConfig.CreateCacheInstance("CacheMain");
            string key = "test-data";
            DistributedCacheEntryOptions cacheEntryOptions = new DistributedCacheEntryOptions();
            cacheEntryOptions.SetAbsoluteExpiration(TimeSpan.FromSeconds(10));
            cache.Set(key, Encoding.UTF8.GetBytes("Hello World"), cacheEntryOptions);
            for (int i = 0; i < 30; i++)
            {
                Thread.Sleep(1000);
                byte[] result = cache.Get(key);
                if (result != null && result.Length > 0)
                {
                    WriteLine(i, Encoding.UTF8.GetString(result));
                }
                else
                {
                    WriteLine(i);
                }
            }
        }

        private static (string da, string db) Main5_DecNumbers(int a, int b)
        {
            return (string.Format("[{0}]", a), string.Format("[{0}]", b));
        }

        private static void MutiParamsDemo()
        {
            (string sa, string sb) = Main5_DecNumbers(100, 200);
            Console.WriteLine(sa + "        " + sb);
        }

        private static void Main6_GetMdf5(string file)
        {
            Process process = Process.Start($"certutil -hashfile {file} md5");
            process.OutputDataReceived += Process_OutputDataReceived;
        }

        private static void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            var a = 123;
            Console.WriteLine(a);
        }

        static void Main(string[] args)
        {

         }
    }
}
