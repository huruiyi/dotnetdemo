﻿using System;
using System.Collections;
using System.Threading;

namespace ConApp
{
    internal class ThreadDemo
    {
        private Thread threadOne;
        private Thread threadTwo;
        private ArrayList stringList;

        private event EventHandler OnNumberClear;//数据删除完成引发的事件

        public static void DemoTest()
        {
            ThreadDemo demo = new ThreadDemo(1000);
            demo.Action();
        }

        public ThreadDemo(int number)
        {
            Random random = new Random(1000000);
            stringList = new ArrayList(number);
            for (int i = 0; i < number; i++)
            {
                stringList.Add(random.Next().ToString());
            }
            threadOne = new Thread(Run);//两个线程共同做一件事情
            threadTwo = new Thread(Run);//两个线程共同做一件事情
            threadOne.Name = "线程1";
            threadTwo.Name = "线程2";
            OnNumberClear += ThreadDemo_OnNumberClear;
        }

        /// <summary>
        /// 开始工作
        /// </summary>
        public void Action()
        {
            threadOne.Start();
            threadTwo.Start();
        }

        /// <summary>
        /// 共同做的工作
        /// </summary>
        private void Run()
        {
            while (true)
            {
                Monitor.Enter(this);//锁定，保持同步
                var stringValue = (string)stringList[0];
                Console.WriteLine(Thread.CurrentThread.Name + "删除了" + stringValue);
                stringList.RemoveAt(0);//删除ArrayList中的元素
                if (stringList.Count == 0)
                {
                    OnNumberClear?.Invoke(this, new EventArgs());//引发完成事件
                }
                Monitor.Exit(this);//取消锁定
                Thread.Sleep(5);
            }

        }

        //执行完成之后，停止所有线程
        private void ThreadDemo_OnNumberClear(object sender, EventArgs e)
        {
            Console.WriteLine("执行完了，停止了所有线程的执行。");
            threadTwo.Abort();
            threadOne.Abort();
        }
    }
}