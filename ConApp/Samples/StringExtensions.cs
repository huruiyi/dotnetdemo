﻿using System;

namespace ConApp
{
    public static class StringExtensions
    {
        public static bool DecimalTryParse(this string s, int num, out decimal result)
        {
            try
            {
                result = Math.Round(decimal.Parse(s), num);
                return true;
            }
            catch
            {
                result = 0;
                return false;
            }
        }

        public static bool UInt16TryParse(this string s, out ushort result)
        {
            try
            {
                result = Convert.ToUInt16(s);
                return true;
            }
            catch (Exception)
            {
                result = 0;
                return false;
            }
        }

        public static bool UInt32TryParse(this string s, out uint result)
        {
            try
            {
                result = Convert.ToUInt32(s);
                return true;
            }
            catch (Exception)
            {
                result = 0;
                return false;
            }
        }

        public static bool UInt64TryParse(this string s, out ulong result)
        {
            try
            {
                result = Convert.ToUInt64(s);
                return true;
            }
            catch (Exception)
            {
                result = 0;
                return false;
            }
        }

        public static bool Int16TryParse(this string s, out short result)
        {
            try
            {
                result = Convert.ToInt16(s);
                return true;
            }
            catch (Exception)
            {
                result = 0;
                return false;
            }
        }

        public static bool Int32TryParse(this string s, out int result)
        {
            try
            {
                result = Convert.ToInt32(s);
                return true;
            }
            catch (Exception)
            {
                result = 0;
                return false;
            }
        }

        public static bool Int64TryParse(this string s, out long result)
        {
            try
            {
                result = Convert.ToInt64(s);
                return true;
            }
            catch (Exception)
            {
                result = 0;
                return false;
            }
        }

        public static bool StringToDate(this string s, out DateTime date)
        {
            try
            {
                date = Convert.ToDateTime(s);
                return true;
            }
            catch (Exception)
            {
                date = DateTime.MinValue;
                return false;
            }
        }

        public static bool DateToString(this DateTime s, string format, out string date)
        {
            try
            {
                date = s.ToString(format);
                return true;
            }
            catch (Exception)
            {
                date = "1990-01-01";
                return false;
            }
        }
    }
}