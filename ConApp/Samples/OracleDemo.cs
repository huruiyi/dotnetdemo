﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;

namespace ConApp
{
    class OracleDemo
    {
        private static String ConStr = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=127.0.0.1)(PORT=1521)))(CONNECT_DATA=(SERVICE_NAME=orcl)));user id=C##HURUIYI;password=huruiyi;";

        public static void Demo1()
        {
            using (OracleConnection conn = new OracleConnection(ConStr))
            {
                conn.Open();
                using (OracleCommand orm = conn.CreateCommand())
                {
                    orm.CommandType = CommandType.StoredProcedure;
                    orm.CommandText = "proc1";
                    int a = orm.ExecuteNonQuery();
                }
            }
        }

        public static void Deo2()
        {
            IDataParameter[] parameters =
            {
                new OracleParameter
                {
                    ParameterName = "v_id",
                    Value = DateTime.Now.Year,
                    Direction = ParameterDirection.Input,
                    DbType = DbType.String
                },
                new OracleParameter
                {
                    ParameterName = "v_name",
                    Value = DateTime.Now.Month,
                    Direction = ParameterDirection.Input,
                    DbType = DbType.String
                }
            };

            using (OracleConnection conn = new OracleConnection(ConStr))
            {
                conn.Open();
                using (OracleCommand orm = conn.CreateCommand())
                {
                    orm.CommandType = CommandType.StoredProcedure;
                    orm.CommandText = "proc2";
                    orm.Parameters.AddRange(parameters);
                    int a = orm.ExecuteNonQuery();
                }
            }
        }

        public static void Demo3()
        {
            IDataParameter[] parameters =
            {
                new OracleParameter
                {
                    ParameterName = "reccount",
                    Direction = ParameterDirection.Output,
                    OracleDbType = OracleDbType.Int32
                },
            };

            using (OracleConnection conn = new OracleConnection(ConStr))
            {
                conn.Open();
                using (OracleCommand orm = conn.CreateCommand())
                {
                    orm.CommandType = CommandType.StoredProcedure;
                    orm.CommandText = "proc3";
                    orm.Parameters.AddRange(parameters);
                    object a = orm.ExecuteNonQuery();
                    Console.WriteLine(orm.Parameters["reccount"].Value);
                }
            }
        }

        public static void Demo4()
        {
            OracleConnection oc = new OracleConnection(ConStr);
            oc.Open();
            OracleCommand ocm = oc.CreateCommand();
            ocm.CommandType = CommandType.StoredProcedure;
            ocm.CommandText = "proc3";
            ocm.Parameters.Add("reccount", OracleDbType.Int32).Direction = ParameterDirection.Output;
            ocm.ExecuteNonQuery();
            string num = ocm.Parameters["reccount"].Value.ToString();
            Console.WriteLine(num);
        }

        public static void Demo5()
        {
            IDataParameter[] parameters =
            {
                new OracleParameter
                {
                    ParameterName = "p$CO_CD",
                    Value = "SMN",
                    Direction = ParameterDirection.Input,
                    OracleDbType = OracleDbType.Varchar2
                },

                new OracleParameter
                {
                    ParameterName = "P$USER",
                    Value = "admin",
                    Direction = ParameterDirection.Input,
                    OracleDbType = OracleDbType.Varchar2
                },
                new OracleParameter
                {
                    ParameterName = "p$CNT_GRP",
                    Value = "SHSZ",
                    Direction = ParameterDirection.Input,
                    OracleDbType = OracleDbType.Varchar2
                },
                new OracleParameter
                {
                    ParameterName = "p$SLIP_NO",

                    Direction = ParameterDirection.Output,
                    OracleDbType = OracleDbType.Int64
                }
            };

            string conStr = "Data Source=(DESCRIPTION=(ADDRESS_LIST=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.1.188)(PORT=1522)))(CONNECT_DATA=(SERVICE_NAME=mcfutf12)));user id=SMNTEST;password=SMNTEST;";

            using (OracleConnection conn = new OracleConnection(conStr))
            {
                conn.Open();
                //using (OracleCommand orm = conn.CreateCommand())
                //{
                //    Console.WriteLine(conn.State);
                //    orm.CommandType = CommandType.StoredProcedure;
                //    orm.CommandText = "SKF_CP_GET_COUNTERNOSP_WEB";
                //    orm.Parameters.AddRange(parameters);
                //    object a = orm.ExecuteScalar();
                //    Console.WriteLine(orm.Parameters["p$SLIP_NO"].Value);
                //}

                using (OracleCommand orm = conn.CreateCommand())
                {
                    using (OracleTransaction myTransaction = conn.BeginTransaction())
                    {
                        orm.Transaction = myTransaction;

                        Console.WriteLine(conn.State);
                        orm.CommandType = CommandType.StoredProcedure;
                        orm.CommandText = "SKF_CP_GET_COUNTERNOSP_WEB";
                        orm.Parameters.AddRange(parameters);
                        object a = orm.ExecuteScalar();
                        myTransaction.Commit();
                    }

                    Console.WriteLine(orm.Parameters["p$SLIP_NO"].Value);
                }
            }
        }

    }
}
