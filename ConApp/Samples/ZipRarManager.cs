﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;

namespace ConApp
{
    public class ZipRarManager
    {
        public static void ExtractFile(string files)
        {
            Console.WriteLine("begin_: " + files);
            FileInfo fileInfo = new FileInfo(files);
            int index = fileInfo.FullName.LastIndexOf(".", StringComparison.Ordinal);
            string ext = Path.GetExtension(files);
            string dst = fileInfo.FullName.Substring(0, index) + "_dst";
            if (ext == ".rar")
            {
                NUnrar.Archive.RarArchive.WriteToDirectory(files, dst);
            }
            else if (ext == ".zip")
            {
                ZipFile.ExtractToDirectory(files, dst);
            }

            Console.WriteLine("end_: " + files);
        }

        /// <summary>
        /// 解压文件
        /// </summary>
        public static void Extract(List<string> zipfiles)
        {
            foreach (string file in zipfiles)
            {
                ExtractFile(file);
            }
        }

        /// <summary>
        /// 删除原始压缩文件,创建新压缩文件
        /// </summary>
        public static void DeleteFileForZip(List<string> dirs)
        {
            int num = 0;
            foreach (string dir in dirs)
            {
                string dest = dir.Replace("_dst", "");
                if (File.Exists(dest + ".zip"))
                {
                    File.Delete(dest + ".zip");
                    Console.WriteLine(++num + "删除:" + dest + ".zip");
                }

                if (File.Exists(dest + ".rar"))
                {
                    File.Delete(dest + ".rar");
                    Console.WriteLine(++num + "删除:" + dest + ".rar");
                }
                ZipFile.CreateFromDirectory(dir, dest + ".zip", CompressionLevel.Optimal, false);
            }
        }
    }
}