﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading;

namespace ConApp
{
    public class AuthorityHelper
    {
        public static string GetMd5(string input, string charset)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] data = md5.ComputeHash(Encoding.GetEncoding(charset).GetBytes(input));
            StringBuilder builder = new StringBuilder(32);
            foreach (byte t in data)
            {
                builder.Append(t.ToString("x2"));
            }
            return builder.ToString();
        }

        ///  <summary>
        /// 
        ///  </summary>
        /// <param name="time">yyyyMMddHHmmss</param>
        /// <param name="content">要加密的字符串</param>
        ///  <returns></returns>
        public static string GetSecurityKey(string time, string content)
        {
            DateTime.TryParse(time, out var stamp);
            StringBuilder securityKey = new StringBuilder();

            #region 年月日时分秒,要加密的字符串

            securityKey.Append(GetMd5(stamp.ToString("yyyy"), "UTF-8"));
            securityKey.Append(GetMd5(stamp.ToString("MM"), "UTF-8"));
            securityKey.Append(GetMd5(stamp.ToString("dd"), "UTF-8"));
            securityKey.Append(GetMd5(stamp.ToString("HH"), "UTF-8"));
            securityKey.Append(GetMd5(stamp.ToString("mm"), "UTF-8"));
            securityKey.Append(GetMd5(stamp.ToString("ss"), "UTF-8"));
            securityKey.Append(GetMd5(content, "UTF-8"));

            #endregion 年月日时分秒,要加密的字符串

            string key = GetMd5(securityKey.ToString(), "UTF-8");

            return key;
        }

        public static bool CheckTokenDemo(string time, string key, string content)
        {
            if (string.IsNullOrEmpty(time) || string.IsNullOrEmpty(key))
            {
                return false;
            }

            DateTime stamp;
            try
            {
                stamp = DateTime.Parse(time);
            }
            catch
            {
                return false;
            }

            TimeSpan timeSpan = new TimeSpan(0, 0, 30);
            if ((DateTime.Now - stamp) <= timeSpan)
            {
                StringBuilder s = new StringBuilder();

                s.Append(GetMd5(stamp.ToString("yyyy"), "UTF-8"));
                s.Append(GetMd5(stamp.ToString("MM"), "UTF-8"));
                s.Append(GetMd5(stamp.ToString("dd"), "UTF-8"));
                s.Append(GetMd5(stamp.ToString("HH"), "UTF-8"));
                s.Append(GetMd5(stamp.ToString("mm"), "UTF-8"));
                s.Append(GetMd5(stamp.ToString("ss"), "UTF-8"));
                s.Append(GetMd5(content, "UTF-8"));

                if (key == GetMd5(s.ToString(), "UTF-8"))
                {
                    return true;
                }
            }

            return false;
        }


        public static void Test()
        {
            long ticks = DateTime.Now.Ticks;
            ticks = 636750693398079395;

            DateTime date1 = new DateTime(ticks);
            string content = "Index";
            string timestamp = date1.ToString();
            string skey = GetSecurityKey(timestamp, content);

            for (int i = 0; i < 100; i++)
            {
                Thread.Sleep(1000);
                bool validate = CheckTokenDemo(timestamp, skey, content);
                Console.WriteLine(validate);
            }
        }
    }
}