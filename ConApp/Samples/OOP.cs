﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConApp.Samples
{
    public class Base
    {
        public virtual bool Validate1()
        {
            Console.WriteLine("Base Validate1() ");
            return true;
        }

        public virtual bool Validate2()
        {
            Console.WriteLine("Base Validate2() ");
            return true;
        }
    }

    public class Child : Base
    {
        public override bool Validate1()
        {
            Console.WriteLine("Child Validate1() ");
            return true;
        }
    }

   public class OOP
    {
        static void Run()
        {
            Child child = new Child();
            child.Validate1();
            child.Validate2();
        }
    }
}
