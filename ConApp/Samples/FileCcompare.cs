﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConApp
{
    public class FileCcompare
    {
        /// <summary>
        /// 获取文件MD5值
        /// </summary>
        /// <param name="fileName">文件绝对路径</param>
        /// <returns>MD5值</returns>
        public static string GetMD5HashFromFile(string fileName)
        {
            try
            {
                FileStream file = new FileStream(fileName, FileMode.Open);
                System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] retVal = md5.ComputeHash(file);
                file.Close();

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < retVal.Length; i++)
                {
                    sb.Append(retVal[i].ToString("x2"));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine("error:  " + fileName + " " + ex.Message);
                return "";
            }
        }

        private class FileMd5Info : IComparable
        {
            public string FullName { get; set; }
            public string FileMd5 { get; set; }

            public int CompareTo(object obj)
            {
                FileMd5Info fileObj = obj as FileMd5Info;
                if (fileObj != null)
                {
                    return string.Compare(FileMd5, fileObj.FileMd5, StringComparison.Ordinal);
                }
                return 0;
            }
        }

        private static readonly List<FileMd5Info> FileList = new List<FileMd5Info>();
        private static readonly List<string> list = new List<string>();

        /// <summary>
        /// 获取文件Md5信息
        /// </summary>
        /// <param name="dir"></param>
        private static void PrintMd5Info(string dir)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(dir);

            FileInfo[] files = directoryInfo.GetFiles();
            foreach (FileInfo fileInfo in files)
            {
                try
                {
                    FileMd5Info file = new FileMd5Info
                    {
                        FullName = fileInfo.FullName,
                        FileMd5 = GetMD5HashFromFile(fileInfo.FullName)
                    };
                    FileList.Add(file);
                    Console.WriteLine(file.FileMd5 + "\t" + file.FullName);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }
            }

            DirectoryInfo[] directoryInfos = directoryInfo.GetDirectories();
            foreach (DirectoryInfo info in directoryInfos)
            {
                PrintMd5Info(info.FullName);
            }
        }

        /// <summary>
        /// 根据Md5信息对文件进行排序
        /// </summary>
        /// <param name="file"></param>
        public static void PrintSortedMd5Info(string file)
        {
            string[] lines = File.ReadAllLines(file);
            foreach (string line in lines)
            {
                string[] item = line.Split('\t');
                if (item.Length == 2)
                {
                    FileList.Add(new FileMd5Info
                    {
                        FileMd5 = item[0],
                        FullName = item[1]
                    });
                }
            }
            Dictionary<string, int> dictionary = new Dictionary<string, int>();
            foreach (FileMd5Info item in FileList)
            {
                if (!dictionary.ContainsKey(item.FileMd5))
                {
                    dictionary[item.FileMd5] = 1;
                }
                else
                {
                    dictionary[item.FileMd5]++;
                }
            }
            foreach (KeyValuePair<string, int> pair in dictionary)
            {
                if (pair.Value > 1)
                {
                    list.Add(pair.Key);
                }
            }

            using (FileStream fileStream = new FileStream(file + "file.sort", FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter streamWriter = new StreamWriter(fileStream))
                {
                    foreach (string item in list)
                    {
                        List<FileMd5Info> results = FileList.FindAll(m => m.FileMd5 == item);
                        streamWriter.WriteLine(item);
                        foreach (FileMd5Info info in results)
                        {
                            streamWriter.WriteLine(info.FullName);
                        }
                        streamWriter.WriteLine();
                    }
                }
            }

            Console.WriteLine("ok");
        }

        /// <summary>
        /// 输出文件信息
        /// </summary>
        public static void PrintFileInfo(string[] dirs)
        {
            foreach (var dir in dirs)
            {
                PrintMd5Info(dir);
            }
        }

        public void FileClear()
        {
            //步骤一
            PrintFileInfo(new[]
            {
                @"D:\Software\baidu"
            });

            //步骤二
            PrintSortedMd5Info(@"D:\Software\baidu\file.txt");
        }
    }
}