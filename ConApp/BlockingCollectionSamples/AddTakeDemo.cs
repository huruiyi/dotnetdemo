﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;

namespace ConApp.BlockingCollectionSamples
{
    internal class AddTakeDemo
    {
        // Demonstrates:
        //      BlockingCollection<T>.Add()
        //      BlockingCollection<T>.Take()
        //      BlockingCollection<T>.CompleteAdding()
        public static void BC_AddTakeCompleteAdding()
        {
            using (BlockingCollection<int> bc = new BlockingCollection<int>())
            {
                // Spin up a Task to populate the BlockingCollection
                using (Task t1 = Task.Factory.StartNew(() =>
                {
                    bc.Add(1);
                    bc.Add(2);
                    bc.Add(3);
                    bc.CompleteAdding();
                }))
                {
                    // Spin up a Task to consume the BlockingCollection
                    using (Task t2 = Task.Factory.StartNew(() =>
                    {
                        try
                        {
                            // Consume consume the BlockingCollection
                            while (true) Console.WriteLine(bc.Take());
                        }
                        catch (InvalidOperationException)
                        {
                            // An InvalidOperationException means that Take() was called on a completed collection
                            Console.WriteLine("That's All!");
                        }
                    }))

                        Task.WaitAll(t1, t2);
                }
            }
        }
    }
}