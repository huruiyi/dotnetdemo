﻿using System.Web.Mvc;
using WebMvcParams.Models;

namespace WebMvcParams.Controllers
{
    public class AccountController : Controller
    {
        // GET: Account
        public ActionResult LogOn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogOn(LogOnModel model, string returnUrl)
        {
            //通过数据库查询验证
            if (model.UserName == "admin" && model.Password == "123456")
            {
                //验证成功，用户名密码正确，构造用户数据（可以添加更多数据，这里只保存用户Id）
                var userData = new MyUserDataPrincipal { UserId = 123 };

                //保存Cookie
                MyFormsAuthentication<MyUserDataPrincipal>.SetAuthCookie(model.UserName, userData, true);

                if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                    && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                {
                    return Redirect(returnUrl);
                }

                return RedirectToAction("Index", "Home");
            }

            // 如果我们进行到这一步时某个地方出错，则重新显示表单
            return View(model);
        }

        public ActionResult Logout()
        {
            return View();
        }
    }
}