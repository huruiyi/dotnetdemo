﻿namespace WebMvcParams.Controllers
{
    public class LogOnModel
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}