﻿using QRCoder;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using WebMvcParams.Models;

namespace WebMvcParams.Controllers
{
    public class Person
    {
        public string Name { get; set; }

        public int Age { get; set; }
    }

    public class HomeController : Controller
    {
        public List<Person> list = new List<Person>();

        public HomeController()
        {
            for (int i = 0; i < 100; i++)
            {
                list.Add(new Person
                {
                    Name = "User" + i.ToString().PadLeft(3, '0'),
                    Age = i
                });
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        [MyAuthorize(Roles = "User", Users = "bomo,toroto")]
        // GET: Home
        public ActionResult QrCode()
        {
            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode("The text which should be encoded.", QRCodeGenerator.ECCLevel.Q);
            QRCode qrCode = new QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);

            MemoryStream ms = new MemoryStream();
            qrCodeImage.Save(ms, ImageFormat.Jpeg);

            return File(ms.ToArray(), "image/jpeg");
        }

        [HttpPost]
        public ActionResult GetParams(List<Person> persons)
        {
            return Json(new { Success = true });
        }

        public ActionResult Items()
        {
            return View();
        }

        public JsonResult GetList(string FINDER = "")
        {
            if (string.IsNullOrWhiteSpace(FINDER))
            {
                return Json(new { Success = false });
            }
            return Json(new { Success = true, Data = list.Where(item => item.Name.Contains(FINDER)) });
        }
    }
}