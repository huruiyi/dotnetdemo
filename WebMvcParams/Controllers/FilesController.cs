﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebMvcParams.Controllers
{
    public class FilesController : Controller
    {
        // GET: Files
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UploadPage()
        {
            return View();
        }
        public ActionResult Upload(HttpPostedFileBase files)
        {
            var requestFiles = Request.Files;

            return Json(new { Success = true, Message = "上传成功"  },JsonRequestBehavior.DenyGet);
        }
    }
}