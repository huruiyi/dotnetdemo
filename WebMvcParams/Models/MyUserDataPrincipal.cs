﻿using System.Collections.Generic;
using System.Security.Principal;

namespace WebMvcParams.Models
{
    //存放数据的用户实体
    public class MyUserDataPrincipal : IPrincipal
    {
  

        public IIdentity Identity
        {
            get;
            private set;
        }

        //数据源

        public int UserId { get; set; }

        //这里可以定义其他一些属性
        public List<int> RoleId { get; set; }

        //当使用Authorize特性时，会调用改方法验证角色
        public bool IsInRole(string role)
        {
            return true;
        }

        //验证用户信息
        public bool IsInUser(string user)
        {
            return true;
        }

        //[ScriptIgnore]    //在序列化的时候忽略该属性
        //public IIdentity Identity { set; }
    }
}